# Foundry Macros
*Made for the Pathfinder 2e system module, and requires the Furnace module.*

**Inspire Courage**:
Allows a player to apply the benefits (attack, damage) to other characters by targeting them, then running the macro. Normally, players cannot make changes to other players' actors. This uses Furnace's GM macro ability, and a two-macro solution to circumvent that. The first macro is used by the PC to gather target and player info, and calls the second macro as a function passing along the info. The second macro must be set up as a GM macro for the GM, and applies the modifies based on the info generated from the first macro.

**Recall Knowledge**:
This is a true secret roll automated macro for Recall Knowledge. This also uses two macros. The first does the heavy lifting (originally done by Dalvyn & Zhamer Kaz, with minor changes from me). It passes the chat results to the GM macro, which whispers them to the GM. It was done this way because I could not find a way for a player macro to output a chat result that would only be seen by the GM.

***Very Important***:

- The names for the GM macros must be exactly what the PC macros are looking for. Enter them as done here, or edit the `macro.execute` function in the PC macros.
- The GM Macros *must* be set to "Execute Macro as GM".

**Trip Action**:
Rolls Athletics with a modifier from the highest potency equipped item with the `trip` trait. Currently no accomodation for multiple attack penalty.

**Add Hero Points**:
Gives 1 Hero point to each selected token, if possible.